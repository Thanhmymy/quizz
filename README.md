# Quizz 

Au cours de la formation Dev Web et Mobile, nous avons comme projet de faire un Quizz en HTML,CSS(Bootstrap) et le Javascript. 
Pour ce quizz, j'ai choisis un modèle simple, je me suis plutôt concentrer sur le fonctionnement du quizz que sur le design. 
Sur ce quizz, on peut y retrouvé 4 questions, avec un timer accompagné pour chaque question (15 secondes). Et à la fin un score, avec un message qui peut varier selon le score qu'on a eu. 

Voici la maquette que j'ai décidé de faire : 
 ![alt text](public/Assets/2021-05-17.png)

Et voici le lien de mon quizz : 

 **Lien:** https://thanhmymy.gitlab.io/quizz
