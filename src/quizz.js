// let bouton = document.querySelector ('#button');
// let mainPage = document.querySelector ('#mainPage');
// let questionPage = document.querySelector('#questionPage')
// let hello = document.querySelector("#hello")
// let question = document.querySelector ("#question")
// let image1 = new Image (400,400)
// let reponse = document.querySelector ('#question .form-check')

// function changerContenu (event){
//     event.preventDefault()
//     console.log('hello')
//     const premierBloc = document.createElement('div')
//     premierBloc.classList.add('card')
//     premierBloc.classList.add('shadow')
//     premierBloc.style.backgroundColor ='#131212'
//     const titre = document.createElement('h1')
//     titre.classList.add('text-center')
//     titre.innerText = 'Quizz Anime'
//     premierBloc.appendChild(titre)
//     hello.appendChild (premierBloc)  
//     mainPage.textContent = " "

//     const deuxiemeBloc = document.createElement('div')
//     deuxiemeBloc.classList.add ('gradient')
//     const titreq = document.createElement('h2')
//     titreq.classList.add('text-center')
//     titreq.innerText = 'Question'
//     deuxiemeBloc.classList.add('container')
//     deuxiemeBloc.style.height ='600px'
//     deuxiemeBloc.appendChild(titreq)
//     question.appendChild (deuxiemeBloc)


//     const troisiemeBloc = document.createElement('div')
//     deuxiemeBloc.appendChild(troisiemeBloc)
//     troisiemeBloc.classList.add('d-flex')
//     const question1 = document.createElement ('h3')
//     question1.innerText = 'Qui est ce personnage ?'
//     deuxiemeBloc.appendChild(question1)
//     troisiemeBloc.appendChild(reponse)

//     image1.src = 'Assets/Luffy.jpg'
//     troisiemeBloc.appendChild(image1)
//     image1.style.marginTop= '30px'

// }
// bouton.addEventListener("click", changerContenu)
let id = 0;
// let data = [
//     ['Qui est ce personnage?',
//         'Assets/luffy.jpg',
//         ['Ace', 'Zoro', 'Luffy', 'Sanji'],
//     ],
//     ['Quel est le nom de cet animé ?',
//         'Assets/totoro.jpg',
//         ['Pokemon', 'Le Monde de Totoro', 'Hamtaro', 'Je sais pas']
//     ],
//     ['Qui sont ces coequipiers?',
//         'Assets/naruto.jpg',
//         ['Itachi Kisame', 'Hinata RockLee', 'Shikamaru Shoji', 'Sasuke Sakura']
//     ],
//     ['De quoi parle cet animé ?',
//         'Assets/conan.jpg',
//         ['De meurtre', 'D\'amour', 'De foot', 'D\'informatique']
//     ],
// ];
let data = [
    {
        question: 'Qui est ce personnage?',
        image: 'Assets/luffy.jpg',
        reponses: ['Ace', 'Zoro', 'Luffy', 'Sanji'],
        bonneReponse: 2
    },
    {
        question: 'Quel est le nom de cet animé ?',
        image:' Assets/totoro.jpg',
        reponses: ['Pokemon', 'Le Monde de Totoro', 'Hamtaro', 'Je sais pas'],
        bonneReponse: 1
    },
    {
        question: 'Qui sont ces coequipiers?',
        image: 'Assets/naruto.jpg',
        reponses: ['Itachi Kisame', 'Hinata RockLee', 'Shikamaru Shoji', 'Sasuke Sakura'],
        bonneReponse: 3
    },
    {
        question: 'De quoi parle cet animé ?',
        image: 'Assets/conan.jpg',
        reponses: ['De meurtre', 'D\'amour', 'De foot', 'D\'informatique'],
        bonneReponse: 0
    },
];

let rep1 = document.querySelector('#rep1')

let rep2 = document.querySelector('#rep2')

let rep3 = document.querySelector('#rep3')

let rep4 = document.querySelector('#rep4')

let rep1value = document.querySelector('#rep1value')

let rep2value = document.querySelector('#rep2value')

let rep3value = document.querySelector('#rep3value')

let rep4value = document.querySelector('#rep4value')

let next = document.querySelector('.next')

let image = document.querySelector('.image')

let question = document.querySelector("#question")

let timer = document.querySelector('#timer')

let input = document.querySelector('input')

let test ;

let reponses = document.getElementsByName('reponse')

let score = 0

let Score = document.querySelector('#score')

let cacher = document.querySelector('#cacher')


function bonneReponse(id) {
    // console.log(data[id][2][0])

    // console.log(data)
    // if (answer === rep1value) {
    //     console.log('true2')

    // console.log(rep1value)

    // }


    // rep1value.value = data[id]['reponses'][0]
    // rep2value.value = data[id]['reponses'][1]
    // rep3value.value = data[id]['reponses'][2]
    // rep4value.value = data[id]['reponses'][3]
    // console.log(reponses)

}


function changerContenu() {
    for (let i = 0; i < reponses.length; i++) {
        if (reponses[i].checked) {
            console.log(reponses[i].value)
            console.log(data[id].bonneReponse)
            if(Number (reponses[i].value) === data[id]['bonneReponse']) {
                score++
                
            }
            

        }
    }

    clearInterval(test);
    if (id < data.length - 1) {

        let i = 15;
        test = setInterval(function () {
            timer.textContent = i--;
            if (i < 0) {
                clearInterval(test);
                changerContenu();
            }
        }, 1000);
        id++
        question.textContent = data[id]['question']
        image.src = data[id]['image']
        rep1.textContent = data[id]['reponses'][0]
        rep2.textContent = data[id]['reponses'][1]
        rep3.textContent = data[id]['reponses'][2] 
        rep4.textContent = data[id]['reponses'][3]
    
        bonneReponse(id);
    }else {
        pageFinale()
    }

}

function pageFinale(){
    let message = document.createElement('p')
    let titreFinale = document.createElement('h1')
    
    Score.appendChild(titreFinale)
    Score.appendChild(message)
    Score.style.display ='flex'
    titreFinale.innerText='Score : ' + score + '/4'
    titreFinale.style.fontFamily = "Caveat, cursive"
    cacher.style.display= 'none'

    if(score < 3){
        message.innerText = 'C\'est triste'
        message.style.fontSize='2em'
        

    }else {
        message.innerText = 'Point faible : trop fort'
        message.style.fontSize='2em'
    }
    
}

next.addEventListener('click', changerContenu)

document.addEventListener("DOMContentLoaded", function () {
    let i = 15;
    test = setInterval(function () {
        timer.textContent = i--;
        if (i < 0) {
            clearInterval(test);
            changerContenu();
        }
    }, 1000);
})

